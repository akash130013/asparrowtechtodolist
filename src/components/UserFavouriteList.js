import React, { useEffect } from 'react'
import { fetchfavouriteList } from '../actions'
import { connect } from 'react-redux'


function UserFavouriteList({ favourite, FavouriteList }) {

    if (favourite.length==0) {
        return <div>No data found</div>
    }

    return (
        <>
            <div className="ui celled list">
               <h1>My Favourite List</h1>
               {favourite.map((item)=>{
                   return  <div className="item">
                   <img className="ui avatar image" src={item.avatar} />
                   <div className="content">
                       <div className="header">{item.first_name} {item.last_name}</div>
                          {item.email}
                   </div>
               </div>
               })}

            </div>
        </>
    )
}


const mapStateToProps = (state) => {
    const { favourite } = state
    return { favourite };
}

export default connect(mapStateToProps)(UserFavouriteList);
