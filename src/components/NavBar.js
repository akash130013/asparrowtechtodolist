import React from 'react'
import { Link} from 'react-router-dom'
import { useLocation } from 'react-router-dom'


export default function NavBar() {

    const location = useLocation();

    return (
        <div>
            <div className="ui clearing segment">
                <h3 className="ui right floated header">
                  <Link to={'/favourite'}>  Go favourite</Link>
                </h3>
                <h3 className="ui left floated header">
                {location.pathname=='/' ? 'My Task':<Link to={'/'}> Go Back </Link>}

               </h3>
            </div>
        </div>
    )
}
