import {
    FETCH_LIST,FETCH_USER,SORTING,FETCH_FAVOURITE,ADD_FAVOURITE,REMOVE_FAVOURITE
} from '../actions/type'
import streams from '../api'




export const fetchList = () => async (dispatch) => {
    const {data} = await streams.get('/users');
    dispatch({
        type: FETCH_LIST,
        payload: data,
    })
}


export const fetchUser = (id) => async (dispatch) => {
    const {data} = await streams.get(`users/${id}`);
   
    dispatch({
        type: FETCH_USER,
        payload: data
    })
}

export const sorting = (option) => async (dispatch) => {
   
    dispatch({
        type: SORTING,
        sort:option,
    })
}

export const fetchfavouriteList = () => async (dispatch) => {
    const {data} = await streams.get('/users');
    dispatch({
        type: ADD_FAVOURITE,
        payload: data,
    })
}

export const setUserFavorite = (userFav) => async (dispatch) => {
      
    dispatch({
        type: ADD_FAVOURITE,
        userFav,
    })
}

export const removeUserFavorite = (id) => async (dispatch) => {
      
    dispatch({
        type: REMOVE_FAVOURITE,
        id,
    })
}